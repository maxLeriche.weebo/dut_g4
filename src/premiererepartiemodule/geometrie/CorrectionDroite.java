/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package premiererepartiemodule.geometrie;

/**
 *
 * @author carrieren
 */
public class CorrectionDroite {
    
    private Point pA;
    private Point pB;
    
    public CorrectionDroite(Point pA, Point pB){
        if((pA != null) && (pB != null) && !pA.esTuIdentiqueA(pB)){
            
            this.pA = pA;
            this.pB = pB;
        }
        else{
            this.pA = new Point();
            this.pB = new Point((float)1.0, (float)1.0);
        }
    }
    
    public CorrectionDroite(float x1, float y1, float x2, float y2){
        this(new Point(x1, y1), new Point(x2, y2));
    }
    
    public CorrectionDroite(){
        //this(null, null);
        
        // this(new Point(), new Point((float)1.0, (float)1.0));
        
        //this((float)0.0, (float)0.0, (float)1.0, (float)1.0);
    }

    @Override
    public String toString() {
        return "CorrectionDroite{" + "pA=" + pA + ", pB=" + pB + '}';
    }
}
