/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package premiererepartiemodule.geometrie;
/**
 *
 * @author carrieren
 */
public class Droite {
    private Point a;
    private Point b;
    
    public Droite(Point a, Point b){ 
        if(a == null && b == null){
            a = new Point(2, 3);
            b = new Point(5, 1);
        }
        else if(a == null){
            a = new Point();
        }
        else if(b == null){
            b = new Point();
        }
        else if(a == b){
            b = new Point();
        }
        else if(a.getAbscisse() == b.getAbscisse() && a.getOrdonnee() == b.getOrdonnee()){
            b = new Point();
        }
        this.a = a;
        this.b = b;
    }
    
    public double longueur(){
       return a.distance(b);
    }
}
