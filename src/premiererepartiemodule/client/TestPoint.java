/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package premiererepartiemodule.client;

import premiererepartiemodule.geometrie.Point;

/**
 *
 * @author carrieren
 */
public class TestPoint {
    
    public static void main(String[] args) {
        
        // geometrie.Point p1;
        
        Point p1= new Point((float)0.0, (float)0.0);
        
        System.out.println("Le point p1 est :" + p1);
        
        
        
        
        
        
        
        
        
        
        
        
        //p1.abscisse = (float)0.5; abscisse est un attribut privé : impossible d'y accéder de l'extérieur de la classe
        //System.out.println(""+p1.getAbscisse());
        
        //p1.afficheToi();
        
        p1.translateToi((float)1.2, (float)3.4);
        
        //p1.afficheToi();
        
        System.out.println("");
        
        Point p2 = new Point((float)4.0,(float)7.0);
        
        //p1.afficheToi();
        //p2.afficheToi();
        
        Point p3 =null;
        
        p1.translateToi(p3);
        
        //p1.afficheToi();
        //p2.afficheToi();
        
        System.out.println(p2);
    }
    
}
