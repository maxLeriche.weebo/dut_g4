/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package premiererepartiemodule.client;
import premiererepartiemodule.geometrie.Point;
import premiererepartiemodule.geometrie.CorrectionDroite;

/**
 *
 * @author carrieren
 */
public class TestCorrectionDroite {
    
    public static void main(String[] args) {
        
        Point unPoint = new Point();
        Point unAutrePoint = new Point((float)3 , (float)4.0);
        
        CorrectionDroite d = new CorrectionDroite(unPoint, unAutrePoint);
        
        System.out.println("unPoint : " + unPoint);
        System.out.println("unAutrePoint : " + unAutrePoint);
        System.out.println("d : " + d);
    }
}
