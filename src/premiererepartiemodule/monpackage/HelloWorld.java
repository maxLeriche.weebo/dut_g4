/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package premiererepartiemodule.monpackage;

import java.util.Arrays;

/**
 *
 * @author carrieren
 */
public class HelloWorld {
    
    public static void main(String[] args) {
        
        System.out.println("Bonjour !");
        System.out.println();
        
        for(int i=0; i<args.length; i++){
            
            System.out.println("case : " + args[i]);
            /* System.out.println("case " + i + " : " + args[i]); */
        }
        System.out.println();
        
        for(String c : args){
            System.out.println("case : " + c);
        }
        
        System.out.println();
        System.out.println(Arrays.asList(args));
        
    }
}
