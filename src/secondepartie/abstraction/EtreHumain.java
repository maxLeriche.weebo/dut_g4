package secondepartie.abstraction;

public abstract class EtreHumain {

	private String nom;
	
	public EtreHumain(String nom) {
		this.nom=nom;
	}
	
	public EtreHumain() {
		this("Nom par defaut");
	}
	
	public String getNom() {
		return nom;
	}
	
	public abstract void vaTamuser();
	//ne pas mettre de code dans la fonction permet d'ecrire ensuite des algos g�n�riques

	@Override
	public String toString() {
		return "EtreHumain [nom=" + nom + "]";
	}
}
