package secondepartie.abstraction;

public class Femme extends EtreHumain {

	private String nomJeuneFille;

	public Femme(String nom, String nomJeuneFille) {
		super(nom);
		this.nomJeuneFille = nomJeuneFille;
	}
	
	public Femme(String nom) {
		super(nom);
		this.nomJeuneFille=nom;
	}
	
	public String getNomJeuneFille() {
		return nomJeuneFille;
	}

	public void setNomJeuneFille(String nomJeuneFille) {
		this.nomJeuneFille = nomJeuneFille;
	}

	@Override
	public String toString() {
		return "Femme [nomJeuneFille=" + nomJeuneFille + "-"+ super.toString()+"]";
	}

	@Override
	public void vaTamuser() {
		System.out.println("\t"+super.getNom()+" it's shopping time !!");	
	}
	
	
}
