package secondepartie.abstraction;

public class Homme extends EtreHumain {

	private boolean barbu;

	public Homme(String nom) {
		super(nom);
		barbu=false;
	}
	
	public boolean isBarbu() {
		return barbu;
	}
	

	@Override
	public String toString() {
		return "Homme [barbu=" + barbu + "-"+super.toString()+"]";
	}

	@Override
	public void vaTamuser() {
		System.out.println("\t"+super.getNom()+" it's game time !!");	
	}
	
	
}
