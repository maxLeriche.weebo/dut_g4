/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartie.informaticien;
import java.awt.Color;
import secondepartie.geometrie.Point;
/**
 *
 * @author Maxime
 */
public class pixel extends Point{
    private Color couleur;
    private boolean visible;
    
    public pixel(Color couleur, boolean visible, float abscisse, float ordonnee) {
        super(abscisse, ordonnee);
        this.couleur = (couleur==null)?Color.BLUE:couleur;
        this.visible=false;
        if (visible)
        {
            allumeToi();
        }
    }
    
    public pixel()
    {
        this(null,false,0,0);
    }
    
    public void allumeToi()
    {
        if (!visible)
        {
            /*
            c'est ici qu'il faudrait allumer physiquement le pixel
            */
            System.out.println("\t Le pixel vient de s'allumer !");
            visible=true;
        }
        
    }
    public void eteintToi()
    {
        if (visible)
        {
            /*
            c'est ici qu'il faudrait eteindre physiquement le pixel
            */
            System.out.println("\t Le pixel vient de s'éteindre !");
            visible=false;
        }
    }

    @Override
    public String toString() {
        return "pixel{" + "couleur=" + couleur + ", visible=" + visible + " " +super.toString() + '}';
    }
    public void translateToi(float abscisse, float lordonnee)
    {
        boolean origine=visible;
        eteintToi();
        super.translateToi(abscisse,lordonnee);
        if (origine)
            {
                allumeToi();
            }
        System.out.println("Je me translatte comme un point");
    }
    @Override
    public void translateToi(Point ou)
    {
        if(ou!=null)
        {
            boolean origine=visible;
            eteintToi();
            super.translateToi(ou);
            if (origine)
                {
                    allumeToi();
                }
        }
        
    }
    
    public void transtaleToi(pixel ou)
    {
        boolean origine=visible;
        eteintToi();
        super.translateToi(ou);
        if (origine)
            {
                allumeToi();
            }
        System.out.println("Je me translatte comme un point");
    }
    public void setOrdonee(float ordonee)
    {
        translateToi(ordonee,getAbscisse());
    }
    public void setAbscisse(float abscisse)
    {
        translateToi(getOrdonnee(),abscisse);
    }
    
    
}
