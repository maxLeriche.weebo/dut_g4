package secondepartie.test;
import secondepartie.abstraction.EtreHumain;
import secondepartie.abstraction.Femme;
import secondepartie.abstraction.Homme;

public class TerstEtreHumain {
	
	public static void main(String[] args) {
		Femme f;
		f=new Femme("Simone");
		
		Homme h=new Homme("Bob");
		
		System.out.println(f.getNomJeuneFille());
		System.out.println(f.getNom());
		f.vaTamuser();
		
		System.out.println(h.isBarbu());
		System.out.println(h.getNom());
		h.vaTamuser();
		
		System.out.println("==========");
		
		EtreHumain e;
		//e=new EtreHumain(); impossible
		e=f;
		System.out.println(e.getNom());
		e.vaTamuser();
		
		
	}
}
