/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartie.decouverteinterface;

/**
 *
 * @author Maxime
 */
public class LecteurDVD implements demarrable,Lecteur{
    private String marques;

    @Override
    public String toString() {
        return "LecteurDVD{" + "marques=" + marques + '}';
    }

    public String getMarques() {
        return marques;
    }

    public LecteurDVD(String marques) {
        this.marques = marques;
    }

    @Override
    public void on() {
        System.out.println("Je suis un Lecteur DVD qui demarre");
        
    }

    @Override
    public void off() {
        System.out.println("Je suis un Lecteur DVD qui s'arrete");
    }

    @Override
    public void play() {
        System.out.println("Je suis un Lecteur qui joue un morceau");
    }

    @Override
    public void stop() {
        System.out.println("Je suis un Lecteur qui arrette de jouer un morceau");
    }
}
