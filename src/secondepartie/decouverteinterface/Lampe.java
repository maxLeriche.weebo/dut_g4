/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartie.decouverteinterface;
import java.awt.Color;

/**
 *
 * @author carrieren
 */
public class Lampe implements demarrable,Comparable{
    
    private Color couleur;
    private int puissance;

    public Lampe(Color couleur, int puissance) {
        this.couleur = (couleur!=null)?couleur:Color.WHITE;
        this.puissance = puissance;
    }

    Lampe(Color BLUE) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Color getCouleur() {
        return couleur;
    }

    public int getPuissance() {
        return puissance;
    }

    @Override
    public String toString() {
        return "Lampe{" + "couleur=" + couleur + ", puissance=" + puissance + '}';
    }

    @Override
    public void on() {
        System.out.println("Je suis une Lampe allumee");
    }

    @Override
    public void off() {
        System.out.println("Je suis une Lampe eteinte");
    }

    @Override
    public int compareTo(Object arg0) {
        Lampe lotre = (Lampe)arg0;
        return this.puissance -lotre.puissance;
    }
    
}
