/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartie.decouverteinterface;
import java.awt.Color;

/**
 *
 * @author Maxime
 */
public class TestInterface {
    public static void main(String[] args) {
        LecteurDVD monLecteurDVD= new LecteurDVD("TOSHIBA");
        Lampe maLampe=new Lampe(Color.BLUE);
        
        System.out.println(monLecteurDVD.getMarques());
        System.out.println(maLampe.getCouleur());
        monLecteurDVD.on();
        monLecteurDVD.off();
        
        maLampe.on();
        
        System.out.println("------------------");
        
        //Lecteur l;
        //l=new Lecteur(); est inerdit
        //l.play();
        //l.stop();
        
        demarrable d=maLampe;
        
        d.off();
        
        
    }
}
