/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartie.decouverteinterface;
import java.util.Arrays;
import java.awt.Color;
import java.util.Comparator;

/**
 *
 * @author carrieren
 */
public class AutreUtilisation {
    
    public static void main(String[] args) {
        
        
        Lampe[] pleinDeLampes = {
            new Lampe(Color.BLUE, 55),
            new Lampe(Color.YELLOW, 20),
            new Lampe(Color.RED, 12),
            new Lampe(Color.GREEN, 37)
        };
        
        for(Lampe uneLampe: pleinDeLampes)
            System.out.println(uneLampe);
        
        System.out.println("------------------");

        Arrays.sort(pleinDeLampes);
        
        for(Lampe uneLampe: pleinDeLampes)
            System.out.println(uneLampe);
        
        System.out.println("------------------");
        
        LecteurDVD[] pleinDeLecteur ={
            new LecteurDVD("SONY"),
            new LecteurDVD("TOSHIBA"),
            new LecteurDVD("LG"),
            new LecteurDVD("SAMSUNG"),
            
        };
        for (LecteurDVD unlecteur:pleinDeLecteur)
        {
            System.out.println(unlecteur);
        }
        
        Arrays.sort(pleinDeLecteur, new Comparator() {
            @Override
            public int compare(Object arg0, Object arg1) {
                LecteurDVD l1=(LecteurDVD)arg0;
                LecteurDVD l2=(LecteurDVD)arg1;
                return l1.getMarques().compareTo(l2.getMarques());
            }
        });
        System.out.println("------------------");
        
        
        for (LecteurDVD unlecteur:pleinDeLecteur)
        {
            System.out.println(unlecteur);
        }
    } 
        
        
}
    

