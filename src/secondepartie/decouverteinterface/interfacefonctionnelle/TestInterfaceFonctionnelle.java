/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartie.decouverteinterface.interfacefonctionnelle;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import secondepartie.decouverteinterface.Lampe;

/**
 *
 * @author Maxime
 */
public class TestInterfaceFonctionnelle {
    public static void main(String[] args) {
        i2 monI2=()->{
            System.out.println("Bonjours");
        };
         monI2.h();
        System.out.println("---------------------");
        
        I3 monI3=(chaine)->{
            System.out.println(chaine);
            return 12;
        };
       int res=monI3.w("toto");
        System.out.println("res="+res);
        
        System.out.println("---------------------");
        
        I4 monI4=(chaine, entier)->{
            System.out.println("La chaine recue:"+ chaine);
            System.out.println("L'entier recu: "+entier);
        };
        
        monI4.v("truc",69);
        System.out.println("---------------------");
        
        List<Lampe> listeLampes =Arrays.asList(
                new Lampe(Color.blue,100),
                new Lampe(Color.CYAN,90),
                new Lampe(Color.BLUE,80),
                new Lampe(Color.BLACK,70)
        );
        
        listeLampes.sort((l1,l2)->{
        
            return l1.getPuissance()-l2.getPuissance();
        });
        
        Consumer<Lampe> afficheur=(l)->{
            System.out.println(l);
        };
        Consumer<Lampe> autreConsommateur=
                (l)->{
                    System.out.println("\t autre consommateur: "+l);
                };
        listeLampes.forEach(afficheur.andThen(autreConsommateur));
    }
}
