/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartie.decouverteinterface.interfacefonctionnelle;

/**
 *
 * @author Maxime
 */
@FunctionalInterface
public interface I4 {
    void v(String s, int i);
    default void methodePardefaut(String s)
    {
        System.out.println("Execution de la methode par defaut de l'interfaxce i4 :" + s);
    }
}
