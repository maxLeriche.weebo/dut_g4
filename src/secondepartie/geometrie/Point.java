/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secondepartie.geometrie;

/**
 *
 * @author carrieren
 */
public class Point {
    
    private float abscisse; 
    private float ordonnee;
    
    public Point(float abscisse, float ordonnee){
        
        this.abscisse = abscisse;
        this.ordonnee = ordonnee;
    }

    public Point() {
        this((float)0.0,(float)0.0);
    }
    
    public float getAbscisse(){
        return abscisse;
    }

    public float getOrdonnee(){
        return ordonnee;
    }
    
    public void translateToi(float abscisse, float lordonnee){
        
        ordonnee = lordonnee;
        this.abscisse = abscisse;
        System.out.println("Je me translatte comme un point");
    }
    
    public void translateToi(Point ou){
        if(ou != null){
            
        this.abscisse = ou.abscisse;
        this.ordonnee = ou.ordonnee;
        
        
        System.out.println("Je me translatte comme un point");
        }
        
    }
    public double distance(Point p){
        double dist;
        if((p.getAbscisse()-this.getAbscisse()) + (p.getOrdonnee()-(this.getOrdonnee())) < 0)
        {
            dist = (p.getAbscisse()-this.getAbscisse()) + (p.getOrdonnee()-(this.getOrdonnee()));
            return Math.abs(dist);
        }
        else{
            return Math.sqrt((p.getAbscisse()-this.getAbscisse()) + (p.getOrdonnee()-(this.getOrdonnee())));
        }      
    }
    
    /*
    public void afficheToi(){
        
        System.out.println("abscisse = " + abscisse  + " - " + "ordonnee = " + ordonnee);
        
    } 
    */
    /*
    public String toString(){
        
        return("abscisse = " + abscisse  + " - " + "ordonnee = " + ordonnee);    
    }   
    */

    @Override
    public String toString() {
        return "Point{" + "abscisse=" + abscisse + ", ordonnee=" + ordonnee + '}';
    }
    
    public boolean esTuIdentiqueA(Point lotre){
        
        if(lotre == null) return false;
        
        if((this.abscisse == lotre.abscisse) && (this.ordonnee == lotre.getOrdonnee())){
            return true;
        }
        else{
            return false;
        }
    }
    
}
