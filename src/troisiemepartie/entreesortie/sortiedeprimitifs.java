/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package troisiemepartie.entreesortie;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
/**
 *
 * @author Maxime
 */
public class sortiedeprimitifs {
    
    public static void main(String[] args) {
        DataOutputStream dos = null;
        try{
            dos = new DataOutputStream(
        new BufferedOutputStream(new FileOutputStream("FicPrimitif"))
        );
            dos.writeInt((43712));
            dos.writeDouble(3.14);
            
            dos.flush();
            dos.close();
        }
        catch(IOException ex){
            System.err.println(ex);
            System.exit(-1);
        }
        
    }
}
